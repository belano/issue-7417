package com.acme.resource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import com.acme.model.Message;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Component
@Path("/hello")
@Produces(APPLICATION_JSON)
@Api(value = "Hello resource", produces = APPLICATION_JSON, tags = {"hello"})
public class HelloResource {

    @GET
    @ApiOperation(value = "foo",
            notes = "Say foo",
            authorizations = {@Authorization(value = "jwt")},
            response = Message.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Okay"),
            @ApiResponse(code = 500, message = "Internal error")
    })
    public Response hello(@ApiParam(name = "name", value = "Some name",
            required = true) @NotBlank(message = "Name cannot be blank") @QueryParam("name") String name) {
        Message message = new Message("Hello " + name);
        return Response.ok(message).build();
    }

}
