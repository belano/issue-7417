package com.acme.config;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.acme.resource.HelloResource;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@Component
public class JerseyConfig extends ResourceConfig {

    @Autowired(required = false)
    private ServletContext servletContext;

    @Value("${spring.jersey.application-path:/}")
    private String apiPath;

    public JerseyConfig() {
        register(HelloResource.class);
    }

    @PostConstruct
    public void init() {
        // Register components where DI is needed
        configureSwagger();
    }

    private void configureSwagger() {
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);

        BeanConfig swaggerConfig = new BeanConfig();
        swaggerConfig.setConfigId("sample");
        swaggerConfig.setTitle("API - Sample App");
        swaggerConfig.setVersion("BETA");        
        swaggerConfig.setSchemes(new String[] {"http"});
        swaggerConfig.setBasePath(this.getBasePath());
        swaggerConfig.setResourcePackage("com.acme.resource");
        swaggerConfig.setPrettyPrint(true);
        swaggerConfig.setScan(true);
    }

    private String getBasePath() {
        if (servletContext != null) {
            return servletContext.getContextPath() + apiPath;
        }
        return apiPath;
    }

}
